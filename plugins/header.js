export default {
    buildHeaders: function (title, description) {
        return {

                "title": title,
                "meta": [
                    { hid: 'description', name: 'description', content: description },
                    { hid: 'ogtitle', property: 'og:title', content: title },
                    { hid: 'twittertitle', name: 'twitter:title', content: title },
                    { hid: 'ogdescription', property: 'og:description', content: description },
                    { name: 'apple-mobile-web-app-title', content: title },
                    { name: 'application-name', content: title },
                ]
        };
    }
};