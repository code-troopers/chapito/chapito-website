const title = 'Chapitô. Le générateur d’application pour événements.'
const description = 'Festival, Foire, Salon, BDE, ou autres organisateurs d’événements, découvrez LE service vous permettant de créer une application sur-mesure sans avoir de connaissance en développement web ni en développement d’application.'
const url = "https://chapi.to"
const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

import Headers from './plugins/header';
let headers = Headers.buildHeaders(
  title,
  description
);

module.exports = {
  mode: 'universal',

  generate: {
    dir: 'public'
  },

  /*
  ** Headers of the page
  */
  head: { ...headers,
    // this htmlAttrs you need
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},

      {hid: 'ogtype', property: 'og:type', content: 'website'},
      {hid: 'ogurl', property: 'og:url', content: url},
      {hid: 'ogimage', property: 'og:image', content: url + '/preview.png'},
      {property: 'og:locale', content: 'fr_FR'},
      // Twitter Card
      {hid: 'twittercard', name: 'twitter:card', content: 'summary'},
      {hid: 'twittersite', name: 'twitter:site', content: '@chapi_to'},
      {hid: 'twitterdescription', name: 'twitter:description', content: description},
      {hid: 'twitterimage', name: 'twitter:image', content: url + '/preview_small.png'},
      {hid: 'twitterimagealt', name: 'twitter:image:alt', content: 'Chapito logo'},
      {name: 'msapplication-TileColor', content: '#ffffff'},
      {name: 'theme-color', content: '#ffffff'},
    ],
    link: [
      {rel: 'stylesheet', href:'//fonts.googleapis.com/css?family=Raleway'},
      {rel: 'apple-touch-icon', sizes: '57x57', href: '/apple-icon-57x57.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '60x60', href: '/apple-icon-60x60.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '72x72', href: '/apple-icon-72x72.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '76x76', href: '/apple-icon-76x76.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '114x114', href: '/apple-icon-114x114.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-icon-120x120.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '144x144', href: '/apple-icon-144x144.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-icon-152x152.png?v=VbJuQyP5t'},
      {rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-icon-180x180.png?v=VbJuQyP5t'},
      {rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png?v=VbJuQyP5t'},
      {rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png?v=VbJuQyP5t'},
      {rel: 'icon', type: 'image/png', sizes: '96x96', href: '/favicon-96x96.png?v=VbJuQyP5t'},
      {rel: 'icon', type: 'image/png', sizes: '192x192', href: '/android-icon-192x192.png?v=VbJuQyP5t'},
      {rel: 'manifest', href: '/site.webmanifest?v=VbJuQyP5t'},
      {rel: 'mask-icon', href: '/safari-pinned-tab.svg?v=VbJuQyP5t', color: '#333333'},
      {rel: 'shortcut icon', href: '/favicon.ico?v=VbJuQyP5t'}

    ]
  },

  /* Uncomment when using gitlab pages
  router: { base },
  */

  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#222333'},
  plugins: [
      { src: '~plugins/vue-carousel', ssr: false }, 
      { src: '~plugins/fb-sdk', ssr: false }
    ],


  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/style.css'
  ],
  server: { host: '0.0.0.0' },

  modules: [
    [
      'nuxt-imagemin',
      {
        optipng: { optimizationLevel: 5 },
        jpegtran: { progressive: true }
      }
    ],
    ['@nuxtjs/google-analytics', {
      id: 'UA-115015210-3'
    }]
  ]
}
